/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:FDL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU Free Documentation License Usage
** Alternatively, this file may be used under the terms of the GNU Free
** Documentation License version 1.3 as published by the Free Software
** Foundation and appearing in the file included in the packaging of
** this file. Please review the following information to ensure
** the GNU Free Documentation License version 1.3 requirements
** will be met: https://www.gnu.org/licenses/fdl-1.3.html.
** $QT_END_LICENSE$
**
****************************************************************************/

/*!

\title Viewer: Command Line Usage
\page viewer-commandline.html
\ingroup qt3dstudio-viewer

The Qt 3D Studio Viewer supports launching a specific presentation or waiting
for incoming connections with a presentation sent remotely.

\section1 Usage

\badcode
    Qt3DViewer [options] [presentation file]
\endcode

When no filename is given, the viewer is launched in remote connection mode.

The following command line options are supported:

\table
    \header
        \li Option
        \li Description
    \row
        \li -h, --help
        \li Displays help for command line usage.
    \row
        \li --sequence
        \li Generates an image sequence. The file argument must be specified. Specifying any of
        the \c {seq-*} arguments implies setting this option.
    \row
        \li --seq-start <ms>
        \li Start time of the sequence in milliseconds. The default value is \c {0}.
    \row
        \li --seq-end <ms>
        \li End time of the sequence in milliseconds. The default value is \c {1000}.
    \row
        \li --seq-fps <fps>
        \li Frames per second for the sequence. The default value is \c {60}.
    \row
        \li --seq-interval <ms>
        \li Time interval between frames in the sequence in milliseconds. The \c {seq-fps} argument
        is ignored if this argument is used.
    \row
        \li --seq-width <pixels>
        \li Width of the image sequence. The default value is \c {1920}.
    \row
        \li --seq-height <pixels>
        \li Height of the image sequence. The default value is \c {1080}.
    \row
        \li --seq-outpath <path>
        \li Output path of the image sequence. The default value is the current directory.
    \row
        \li --seq-outfile <file>
        \li Output filename base for the image sequence. The default value is derived from the
        presentation filename.
    \row
        \li --connect <port>
        \li If this parameter is specified, the viewer is started in connection mode. The default
        value is \c {36000}.
    \row
        \li --fullscreen
        \li Starts the viewer in fullscreen mode.
    \row
        \li --maximized
        \li Starts the viewer in maximized mode.
    \row
        \li --windowgeometry <geometry>
        \li Specifies the initial window geometry using the X11-syntax. For example:
        \c {1000x800+50+50}
    \row
        \li --mattecolor <color>
        \li Specifies custom matte color using #RRGGBB syntax. For example, white matte:
        \c {#ffffff}
    \row
        \li --showstats
        \li Show render statistics on screen.
    \row
        \li --scalemode <center|fit|fill>
        \li Specifies scaling mode. The default value is \c {center}.
    \row
        \li --stereomode <mono|topbottom|leftright>
        \li Specifies stereo mode. The default value is \c {mono}.
    \row
        \li --stereoeyeseparation <separation>
        \li Specifies stereo eye separation. The default value is \c {0.4}.
    \row
        \li --enableprogressivestereo
        \li Enables progressive stereoscopic rendering.
    \row
        \li --convert-shader-cache <filename>
        \li Convert base64 dump to shader cache file.
    \row
        \li -v, --variants <variants>
        \li Gives list of variant groups and variants to be loaded from the presentation. For
        example \c {VarGroupA:var1,VarGroupB:var4}.
\endtable
*/
